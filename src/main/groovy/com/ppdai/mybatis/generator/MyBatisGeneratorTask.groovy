package com.ppdai.mybatis.generator

import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.tasks.TaskAction
import org.mybatis.generator.api.MyBatisGenerator
import org.mybatis.generator.api.ProgressCallback
import org.mybatis.generator.api.VerboseProgressCallback
import org.mybatis.generator.config.xml.ConfigurationParser
import org.mybatis.generator.internal.DefaultShellCallback
import org.mybatis.generator.internal.NullProgressCallback

/**
 * @author jearton
 * @since 2017/8/15
 */
class MyBatisGeneratorTask extends DefaultTask {

    def LOG_PREFIX = '[MBG]'

    static JDBC_DRIVER = 'jdbc.driver'
    static JDBC_URL = 'jdbc.url'
    static JDBC_USERNAME = 'jdbc.username'
    static JDBC_PASSWORD = 'jdbc.password'

    static FILE_BASE_PACKAGE = 'file.basePackage'
    static FILE_MODEL_DIR = 'file.modelDir'
    static FILE_MODEL_PACKAGE = 'file.modelPackage'
    static FILE_XML_DIR = 'file.xmlDir'
    static FILE_XML_PACKAGE = 'file.xmlPackage'
    static FILE_CLIENT_DIR = 'file.clientDir'
    static FILE_CLIENT_PACKAGE = 'file.clientPackage'

    @TaskAction
    void generatorAction() {
        println "${LOG_PREFIX} BEGIN"

        // 读取配置参数
        def generatorExtension = this.generatorExtension
        def xmlContext = this.xmlContext
        if (generatorExtension.verbose) {
            xmlContext.each { println "${LOG_PREFIX} XML CONTEXT: [$it.key = $it.value]" }
        }

        // 验证配置参数
        validate(xmlContext, generatorExtension)

        // 执行官方MBG
        executeGenerator(xmlContext, generatorExtension)

        println "${LOG_PREFIX} END"
    }

    MybatisGeneratorExtension getGeneratorExtension() {
        def config = new MybatisGeneratorExtension(project)
        config.configFile = project.mbg.configFile
        config.overwrite = project.mbg.overwrite
        config.verbose = project.mbg.verbose
        config.debug = project.mbg.debug
        config
    }

    Map<?, ?> getXmlContext() {
        [
                (JDBC_DRIVER)        : project.mbg.jdbc.driver,
                (JDBC_URL)           : project.mbg.jdbc.url,
                (JDBC_USERNAME)      : project.mbg.jdbc.username,
                (JDBC_PASSWORD)      : project.mbg.jdbc.password,
                (FILE_BASE_PACKAGE)  : project.group,
                (FILE_MODEL_DIR)     : project.mbg.file.modelDir,
                (FILE_MODEL_PACKAGE) : project.mbg.file.modelPackage,
                (FILE_XML_DIR)       : project.mbg.file.xmlDir,
                (FILE_XML_PACKAGE)   : project.mbg.file.xmlPackage,
                (FILE_CLIENT_DIR)    : project.mbg.file.clientDir,
                (FILE_CLIENT_PACKAGE): project.mbg.file.clientPackage
        ].findAll { it.value != null }
    }

    static void validate(Map<?, ?> xmlContext, MybatisGeneratorExtension generatorExtension) {
        def generatorConfigFile = generatorExtension.generatorConfigFile
        if (!generatorConfigFile.exists()) {
            throw new GradleException("configFile[$generatorConfigFile.absolutePath] not exists")
        }

        if (!xmlContext[JDBC_DRIVER]) {
            println xmlContext[JDBC_DRIVER]
            throw new GradleException("\${$JDBC_DRIVER} may not be empty")
        }

        if (!xmlContext[JDBC_URL]) {
            throw new GradleException("\${$JDBC_URL} may not be empty")
        }

        if (!xmlContext[JDBC_USERNAME]) {
            throw new GradleException("\${$JDBC_USERNAME} may not be empty")
        }

        if (!xmlContext[FILE_BASE_PACKAGE]) {
            throw new GradleException("\${$FILE_BASE_PACKAGE} may not be empty, please set project.group=you package")
        }

        if (!xmlContext[FILE_MODEL_PACKAGE]) {
            throw new GradleException("\${$FILE_MODEL_PACKAGE} may not be empty")
        }

        if (!xmlContext[FILE_XML_PACKAGE]) {
            throw new GradleException("\${$FILE_XML_PACKAGE} may not be empty")
        }

        if (!xmlContext[FILE_CLIENT_PACKAGE]) {
            throw new GradleException("\${$FILE_CLIENT_PACKAGE} may not be empty")
        }
    }

    void executeGenerator(Map<?, ?> xmlContext, MybatisGeneratorExtension generatorExtension) {
        // 当前执行环境下的父级目录
        def projectDir = project.projectDir.path.replace(new File("").absolutePath, '.') + File.separator
        if (!new File(projectDir).exists()) {
            throw new GradleException("projectDir[$projectDir] not exists")
        }

        // 初始化配置
        def extra = new Properties()
        extra.putAll(xmlContext)
        def warnings = []
        def config = new ConfigurationParser(extra, warnings).parseConfiguration(generatorExtension.generatorConfigFile)
        config.contexts.each {
            it.javaClientGeneratorConfiguration.targetProject = projectDir + it.javaClientGeneratorConfiguration.targetProject
            it.javaModelGeneratorConfiguration.targetProject = projectDir + it.javaModelGeneratorConfiguration.targetProject
            it.sqlMapGeneratorConfiguration.targetProject = projectDir + it.sqlMapGeneratorConfiguration.targetProject
        }

        // 执行过程钩子
        ProgressCallback progressCallback = generatorExtension.verbose ? new VerboseProgressCallback() {
            @Override
            void startTask(String taskName) {
                println "${LOG_PREFIX} ${taskName}"
            }
        } : new NullProgressCallback()
        def shellCallback = new DefaultShellCallback(generatorExtension.overwrite)

        // 执行生成器
        new MyBatisGenerator(config, shellCallback, warnings).generate(progressCallback)

        // 打印执行过程警告
        if (generatorExtension.debug) {
            warnings.each { println "${LOG_PREFIX} [WARNING] ${it}" }
        }
    }
}
