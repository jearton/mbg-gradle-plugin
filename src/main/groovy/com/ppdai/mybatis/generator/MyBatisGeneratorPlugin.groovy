package com.ppdai.mybatis.generator

import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 * @author jearton
 * @since 2017/2/26
 */
class MyBatisGeneratorPlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {
        project.extensions.create("mbg", MybatisGeneratorExtension, project)
        project.mbg.extensions.create("jdbc", JdbcExtension)
        project.mbg.extensions.create("file", FileExtension, project)
        project.task("mbg", type: MyBatisGeneratorTask)
    }
}